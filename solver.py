class MfpSolver(object):

	"""Monotone Framework Solver"""
	def __init__(self, flow, kill, gen, debug=True):
		super(MfpSolver, self).__init__()
		self.flow = flow
		self.kill = kill
		self.gen = gen
		self.debug = debug

		self.W = []

		self.A = [set([]) for i in range(len(kill))]
		if not len(kill) == len(gen):
			raise ValueError("kill and gen lists must be of equal length")

		self.initialize()

	def nodes(self):
		n = set()
		for ll in self.flow:
			(l,lp) = ll
			if self.debug:
				print "fl: ({} {})".format(l,lp)
			n = n | set([l, lp])
		if self.debug:
			print "nodes: {}".format(str(n))
		return n

	def worklist(self, step):
		w = []
		for (l, lp) in self.W:
			w.append("({},{})".format(l+1,lp+1))
		print("\n\\vspace{0.1in}")
		print("The worklist state before iteration {}: $\\{{{}\\}}$".format(step, ",".join(w)))

	''' l is index '''
	def fl(self, l):
		return (self.A[l] - self.kill[l]) | self.gen[l] 

	def initialize(self):
		self.W = self.edges()

	def iterate(self):
		step = 0
		if self.debug:
			print "Iterating"
		while self.W:
			step += 1
			self.worklist(step)
			if self.debug:
				print "self.W"
			(l, l_prime) = self.W.pop(0)
			l_analysis = self.fl(l)
			if not self.superset(l_analysis, self.A[l_prime]):
				if self.debug:
					print "setop"
				self.A[l_prime] = self.setop(self.A[l_prime], l_analysis)
				new_nodes = [(lp, lpp) for (lp, lpp) in self.edges() if (lp == l_prime and (lp,lpp) not in self.W)]
				new_nodes.reverse()
				self.W.reverse()
				self.W.extend(new_nodes)
				self.W.reverse()	 
				self.result("After step {} -- edge $({},{})$".format(step, l+1, l_prime+1))
			else:
				print "\nNo changes made in the iteration step {} for path $({},{})$".format(step, l+1, l_prime+1)

	def result(self, message="Final results"):
		raise NotImplementedError()

	def set_to_s(self, s):
		if not s:
			return "\\varnothing"
		else:
			return "\\{{ {} \\}}".format(",".join(s))

	def initialize_nodes(self):
		raise NotImplementedError()

	def E(self):
		raise NotImplementedError()

	def superset(self, set_orig, other):
		raise NotImplementedError()

	def setop(self, set_orig, other):
		raise NotImplementedError()


class AESolver(MfpSolver):

	"""docstring for AESolver"""
	def __init__(self, expr, flow, kill, gen, debug=True):
		self.expr = expr
		super(AESolver, self).__init__(flow, kill, gen, debug)

	def initialize(self):
		super(AESolver, self).initialize()

		for i in self.nodes():
			if i in self.E():
				self.A[i] = set([])
			else:
				self.A[i] = set(self.expr)

	def edges(self):
		return list(self.flow)

	''' We agree that the starting node is always named 0 '''
	def E(self):
		return set([0])

	''' Is superset of? '''
	def superset(self, set_orig, other):
		return set_orig >= other

	''' Intersection '''
	def setop(self, set_orig, other):
		return set_orig & other

	def result(self, message="Final results"):
		print("")
		print("{}:".format(message))
		for i in self.nodes():
			print("")
			print "$AE_{{\\circ}}({}): {}$\\\\".format(i+1, self.set_to_s(self.A[i]))
			print "$AE_{{\\bullet}}({}): {}$".format(i+1, self.set_to_s(self.fl(i)))

class LVSolver(MfpSolver):

	"""docstring for LVSolver"""
	def __init__(self, var, end, flow, kill, gen, debug=True):
		self.var = set(var)
		self.end = list(end)
		super(LVSolver, self).__init__(flow, kill, gen, debug)

	def initialize(self):
		super(LVSolver, self).initialize()

		for i in self.nodes():
			self.A[i] = set([])
	
	''' Reverse edge flow (also reverse the edge list so that we start from the end) '''
	def edges(self):
		edges = [(ri,li) for (li, ri) in self.flow]
		edges.reverse()
		return edges

	''' We agree that there is only one final node (as it was in exercise) '''
	def E(self):
		return set(self.end)

	''' Subset in this case '''
	def superset(self, set_orig, other):
		return set_orig <= other

	''' Union '''
	def setop(self, set_orig, other):
		return set_orig | other

	def result(self, message="Final results"):		
		print("")
		print("{}:".format(message))
		for i in self.nodes():
			print("")
			print "$LV_{{\\circ}}({}): {}$\\\\".format(i+1, self.set_to_s(self.fl(i)))
			print "$LV_{{\\bullet}}({}): {}$".format(i+1, self.set_to_s(self.A[i]))

