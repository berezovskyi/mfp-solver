import sys
from solver import AESolver, LVSolver

flow = [(0,1),(0,2),(1,3),(2,3),(3,4),(3,5),(4,3)]


def solve_ae():
	expr = set([ "5*x", "(5*x)-y", "y+4", "(y+4)*x", "(5*x)*(y+4)", "x+y", "y/2", "y/2-x", "(5*x)+(y+4)"])
	kill = [set(), set(), set(), set(), set(["(5*x)-y", "y+4", "(y+4)*x", "(5*x)*(y+4)", "x+y", "y/2", "y/2-x", "(5*x)+(y+4)"]), set()]
	gen = [
		set(["5*x", "(5*x)-y"]),
		set(["y+4", "(y+4)*x"]),
		set(["5*x", "(5*x)*(y+4)", "y+4"]),
		set(["x+y"]),
		set(),
		set(["5*x", "(5*x)+(y+4)", "y+4"])
	]

	solver = AESolver(expr, flow, kill, gen, False)
	solver.iterate()
	solver.result()

def solve_lv():
	var = set(["x", "y", "z"])
	kill = [set(), set(["z"]), set(["z"]), set(), set(["y"]), set(["z"])]
	gen = [set(["x", "y"]), set(["x", "y"]), set(["x", "y"]), set(["x", "y"]), set(["x", "y"]), set(["x", "y"])]

	solver = LVSolver(var, [5], flow, kill, gen, False)
	solver.iterate()
	solver.result()

def error():
	print("Error")

def main():
	if len(sys.argv) == 2:
		flag = sys.argv[1]
		if flag == "--ae":
			solve_ae()
		elif flag == "--lv":
			solve_lv()
		else:
			error()
	else:
		error()

if __name__ == '__main__':
	main()
